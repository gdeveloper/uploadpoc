README

1.Run the script in Microsoft SQL Server.

2.Open Project sln (Solution File) - Contains two projects, the data file uploader and the API.

3.In project UploadPOC, in web.config file you have the following key:

<add key="UseEntityFramework" value="false"/>

I developed two ways of accessing to database if that flag is with value true it means the application will use the entity framework to deal with the database, if that flag is with value false it means the application will use direct queries.
