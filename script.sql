GO

/****** Object:  Database [UploadPOC_DB]    Script Date: 22/12/2019 23:42:46 ******/
CREATE DATABASE [UploadPOC_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UploadPOC_DB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UploadPOC_DB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UploadPOC_DB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UploadPOC_DB_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UploadPOC_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [UploadPOC_DB] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET ARITHABORT OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [UploadPOC_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [UploadPOC_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET  DISABLE_BROKER 
GO

ALTER DATABASE [UploadPOC_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [UploadPOC_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [UploadPOC_DB] SET  MULTI_USER 
GO

ALTER DATABASE [UploadPOC_DB] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [UploadPOC_DB] SET DB_CHAINING OFF 
GO

ALTER DATABASE [UploadPOC_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [UploadPOC_DB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [UploadPOC_DB] SET  READ_WRITE 
GO

/*****--------------------------------------------------------------------------------------****/

USE [UploadPOC_DB]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 22/12/2019 23:42:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[TransactionId] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CurrencyCode] [varchar](3) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Status] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00001', CAST(250.30 AS Decimal(18, 2)), N'EUR', CAST(N'2019-12-19T18:13:45.280' AS DateTime), N'Approved')
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00002', CAST(200.80 AS Decimal(18, 2)), N'USD', CAST(N'2019-12-19T18:14:14.857' AS DateTime), N'Rejected')
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00003', CAST(290.80 AS Decimal(18, 2)), N'USD', CAST(N'2019-01-23T13:45:10.000' AS DateTime), N'Done')
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00004', CAST(10000.97 AS Decimal(18, 2)), N'EUR', CAST(N'2019-01-26T16:09:15.000' AS DateTime), N'Rejected')
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00005', CAST(716.64 AS Decimal(18, 2)), N'USD', CAST(N'2019-01-27T13:45:10.000' AS DateTime), N'Done')
INSERT [dbo].[Transactions] ([TransactionId], [Amount], [CurrencyCode], [TransactionDate], [Status]) VALUES (N'Inv00006', CAST(10000.94 AS Decimal(18, 2)), N'EUR', CAST(N'2019-01-24T16:09:15.000' AS DateTime), N'Rejected')
