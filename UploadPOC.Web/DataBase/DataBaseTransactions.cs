﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using UploadPOC.Web.Helper;
using UploadPOC.Web.Models;

namespace UploadPOC.Web.DataBase
{
    public class DataBaseTransactions
    {
        public void InsertDataFromQueries(List<Transaction> transactions)
        {
            string dbInsertQuery = "INSERT INTO [dbo].[Transactions]([TransactionId],[Amount],[CurrencyCode],[TransactionDate],[Status])VALUES(@transactionId, @amount, @currencyCode, @transactionDate, @status)";
            string conString = WebConfigurationManager.ConnectionStrings["UploadPOC_Queries"].ConnectionString;

            SqlConnection connection = new SqlConnection(conString);
            try
            {
                foreach (Transaction trans in transactions)
                {
                    string transactionId = trans.TransactionId;
                    string currencyCode = trans.PaymentDetailsObj.CurrencyCode;
                    string status = trans.Status;
                    using (SqlCommand cmd = new SqlCommand(dbInsertQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("TransactionId", transactionId);
                        SqlParameter amount_P = new SqlParameter();
                        amount_P.ParameterName = "Amount";
                        amount_P.SqlDbType = System.Data.SqlDbType.Decimal;
                        amount_P.Value = trans.PaymentDetailsObj.Amount;
                        cmd.Parameters.Add(amount_P);
                        cmd.Parameters.AddWithValue("CurrencyCode", currencyCode);
                        SqlParameter transactionDate_P = new SqlParameter();
                        transactionDate_P.ParameterName = "TransactionDate";
                        transactionDate_P.SqlDbType = System.Data.SqlDbType.DateTime;
                        transactionDate_P.Value = trans.TransactionDate;
                        cmd.Parameters.Add(transactionDate_P);
                        cmd.Parameters.AddWithValue("Status", status);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                connection.Close();
            }



        }

        public void InsertDataUsingEntityFramework(List<Transactions> transactions)
        {
            devgoncaloribeiro_UploadPOC_DBEntities db = new devgoncaloribeiro_UploadPOC_DBEntities();
            try
            {
                foreach (Transactions trans in transactions)
                {
                    db.Transactions.Add(trans);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
        }
    }
}