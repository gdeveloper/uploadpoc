﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using UploadPOC.Web.DataBase;
using UploadPOC.Web.Helper;
using UploadPOC.Web.Models;

namespace UploadPOC.Web.Controllers
{
    public class UploadController : Controller
    {
        bool useEntityFramework = Convert.ToBoolean(WebConfigurationManager.AppSettings["UseEntityFramework"]);
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            Utils utils = new Utils();
            DataBaseTransactions databaseTransactions = new DataBaseTransactions();
            List<Transaction> transactions = new List<Transaction>();
            try
            {
                string fileExtension = utils.GetFileExtension(file.FileName);

                if ((file.ContentLength > 0) && ((fileExtension == "xml") || (fileExtension == "csv")) && (file.ContentLength / 1024f) < 1)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    FileInfo fileInfo = new FileInfo(_path);
                    file.SaveAs(_path);
                    
                    switch (fileExtension)
                    {
                        case "xml":
                            transactions = utils.XMLFileToModel(_path);
                            break;
                        case "csv":
                            transactions = utils.CSVFileToModel(_path);
                            break;
                        default:
                            break;
                    }

                    ViewBag.Message = "File Uploaded Successfully!!";
                    Log.Warn(ViewBag.Message);

                    System.IO.File.Delete(_path);

                    bool isValid = utils.CustomFieldsValidations(transactions);

                    if(!isValid)
                    {
                        ViewBag.Message = "Bad Request";
                        Log.Warn(ViewBag.Message);
                    }
                    else
                    {
                        if(useEntityFramework)
                        {
                            List<Transactions> dbTransactions = utils.ConvertToDbModel(transactions);
                            databaseTransactions.InsertDataUsingEntityFramework(dbTransactions);
                        }
                        else
                        {
                            databaseTransactions.InsertDataFromQueries(transactions);
                        }
                    }
                }
                else if((file.ContentLength / 1024f) > 1)
                {
                    throw new Exception("File exceeds 1Mb.");
                }
                else
                {
                    ViewBag.Message = "Unknown format.";
                    Log.Warn(ViewBag.Message);
                }

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Message = ex.Message;
                Log.Error(ex.ToString());
                return View();
            }
        }
    }
}