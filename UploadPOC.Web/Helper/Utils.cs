﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using UploadPOC.Web.Models;

namespace UploadPOC.Web.Helper
{
    public class Utils
    {
        public List<Transactions> ConvertToDbModel(List<Transaction> transactions)
        {
            List<Transactions> dbTransactions = new List<Transactions>();
            try
            {
                foreach (Transaction trans in transactions)
                {
                    Transactions transaction = new Transactions();
                    transaction.TransactionId = trans.TransactionId;
                    transaction.TransactionDate = trans.TransactionDate;
                    transaction.Status = trans.Status;
                    transaction.CurrencyCode = trans.PaymentDetailsObj.CurrencyCode;
                    transaction.Amount = trans.PaymentDetailsObj.Amount;
                    dbTransactions.Add(transaction);
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
            return dbTransactions;
        }

        public List<Transaction> XMLFileToModel(string filePath)
        {
            List<Transaction> productList = new List<Transaction>();
            try
            {
                string xml = File.ReadAllText(filePath);

                XmlSerializer serializer = new XmlSerializer(typeof(List<Transaction>), new XmlRootAttribute("Transactions"));
                StringReader stringReader = new StringReader(xml);
                productList = (List<Transaction>)serializer.Deserialize(stringReader);
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }
            return productList;
        }

        public List<Transaction> CSVFileToModel(string filePath)
        {
            List<Transaction> values = File.ReadAllLines(filePath)
                   .Skip(1)
                   .Select(v => Transaction.FromCsv(v))
                   .ToList();

            return values;
        }

        public bool CustomFieldsValidations(List<Transaction> transactionsList)
        {
            bool isValid = false;

            try
            {
                foreach (Transaction transaction in transactionsList)
                {
                    ValidationContext context = new ValidationContext(transaction, null, null);
                    List<ValidationResult> result = new List<ValidationResult>();
                    isValid = Validator.TryValidateObject(transaction, context, result, true);

                    switch(transaction.Status.ToLower())
                    {
                        case "failed":
                            transaction.Status = "Rejected";
                            break;
                        case "finished":
                            transaction.Status = "Done";
                            break;
                    }

                    if (!(transaction.Status == "Approved") && !(transaction.Status == "Rejected") && !(transaction.Status == "Done"))
                    {
                        if (isValid)
                        {
                            isValid = false;
                            throw new Exception("Status should be 'Approved', 'Rejected' or 'Done'.");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }

            return isValid;
        }

        public string GetFileExtension(string fileName)
        {
            string[] fileNameArr = fileName.Split('.');

            if(fileNameArr.Count() == 2)
            {
                return fileNameArr[1];
            }

            return "";
        }
    }
}