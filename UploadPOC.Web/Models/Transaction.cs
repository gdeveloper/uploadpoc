﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using UploadPOC.Web.Helper;

namespace UploadPOC.Web.Models
{
    public class Transaction
    {
        [Required]
        [StringLength(50)]
        [XmlAttribute("id")]
        public string TransactionId { get; set; }

        [Required]
        [XmlElement("PaymentDetails")]
        public PaymentDetails PaymentDetailsObj { get; set; }

        [Required]
        [XmlElement("TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [Required]
        [XmlElement("Status")]
        public string Status { get; set; }

        public static Transaction FromCsv(string csvLine)
        {
            Transaction transaction = new Transaction();

            try
            {
                string[] values = csvLine.Split(',');

                transaction.TransactionId = values[3];
                transaction.Status = values[2];
                transaction.TransactionDate = Convert.ToDateTime(values[0]);

                string[] paymentDetailsValues = values[1].Split('|');
                transaction.PaymentDetailsObj = new PaymentDetails();
                paymentDetailsValues[0] = paymentDetailsValues[0].Replace('.', ',');
                transaction.PaymentDetailsObj.Amount = Convert.ToDecimal(paymentDetailsValues[0]);
                transaction.PaymentDetailsObj.CurrencyCode = paymentDetailsValues[1];
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                throw ex;
            }

            return transaction;
        }
    }

    public class PaymentDetails
    {
        [Required]
        [XmlElement("Amount")]
        public decimal Amount { get; set; }

        [Required]
        [StringLength(3)]
        [XmlElement("CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}