﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using UploadPOC.WebAPI.Models;
using UploadPOC.WebAPI.Utils;

namespace UploadPOC.WebAPI.Controllers
{
    [Route("[controller]")]
    public class TransactionsController : Controller
    {
        private string connectionString;
        private string selectQuery = "SELECT TransactionId, Amount, CurrencyCode, TransactionDate, Status FROM Transactions {0}";

        public TransactionsController()
        {
            connectionString = ConfigurationManager.AppSetting["ConnectionStrings:TransactionsCon"];
        }

        [HttpGet]
        [Route("GetAllTransactionsByCurrency")]
        public List<Transaction> GetAllTransactionsByCurrency(string currency)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            List<Transaction> transactions = new List<Transaction>();
            string filter = string.Format("WHERE CurrencyCode='{0}'", currency);
            selectQuery = string.Format(selectQuery, filter);

            try
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Check is the reader has any rows at all before starting to read.
                        if (reader.HasRows)
                        {
                            // Read advances to the next row.
                            while (reader.Read())
                            {
                                Transaction trans = new Transaction();
                                trans.Id = reader.GetString(reader.GetOrdinal("TransactionId"));
                                trans.Payment = string.Concat(reader.GetDecimal(reader.GetOrdinal("Amount")), " ", reader.GetString(reader.GetOrdinal("CurrencyCode")));

                                switch (reader.GetString(reader.GetOrdinal("Status")).ToLower())
                                {
                                    case "approved":
                                        trans.Status = 'A';
                                        break;
                                    case "rejected":
                                        trans.Status = 'R';
                                        break;
                                    case "done":
                                        trans.Status = 'D';
                                        break;
                                    default:
                                        break;
                                }

                                transactions.Add(trans);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch { }
            finally
            {
                connection.Close();
            }

            return transactions;
        }

        [HttpGet]
        [Route("GetAllTransactionsByDateRange")]
        public List<Transaction> GetAllTransactionsByDateRange(string startDate, string endDate)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            List<Transaction> transactions = new List<Transaction>();
            string filter = string.Format("WHERE TransactionDate BETWEEN '{0}' AND '{1}'", startDate, endDate);
            selectQuery = string.Format(selectQuery, filter);

            try
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Transaction trans = new Transaction();
                                trans.Id = reader.GetString(reader.GetOrdinal("TransactionId"));
                                trans.Payment = string.Concat(reader.GetDecimal(reader.GetOrdinal("Amount")), " ", reader.GetString(reader.GetOrdinal("CurrencyCode")));

                                switch (reader.GetString(reader.GetOrdinal("Status")).ToLower())
                                {
                                    case "approved":
                                        trans.Status = 'A';
                                        break;
                                    case "rejected":
                                        trans.Status = 'R';
                                        break;
                                    case "done":
                                        trans.Status = 'D';
                                        break;
                                    default:
                                        break;
                                }

                                transactions.Add(trans);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch(Exception ex)
            { }
            finally
            {
                connection.Close();
            }
            return transactions;
        }

        [HttpGet]
        [Route("GetAllTransactionsByStatus")]
        public List<Transaction> GetAllTransactionsByStatus(string status)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            List<Transaction> transactions = new List<Transaction>();
            string filter = string.Format("WHERE Status='{0}'", status);
            selectQuery = string.Format(selectQuery, filter);

            try
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Check is the reader has any rows at all before starting to read.
                        if (reader.HasRows)
                        {
                            // Read advances to the next row.
                            while (reader.Read())
                            {
                                Transaction trans = new Transaction();
                                trans.Id = reader.GetString(reader.GetOrdinal("TransactionId"));
                                trans.Payment = string.Concat(reader.GetDecimal(reader.GetOrdinal("Amount")), " ", reader.GetString(reader.GetOrdinal("CurrencyCode")));

                                switch (reader.GetString(reader.GetOrdinal("Status")).ToLower())
                                {
                                    case "approved":
                                        trans.Status = 'A';
                                        break;
                                    case "rejected":
                                        trans.Status = 'R';
                                        break;
                                    case "done":
                                        trans.Status = 'D';
                                        break;
                                    default:
                                        break;
                                }

                                transactions.Add(trans);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch { }
            finally
            {
                connection.Close();
            }

            return transactions;
        }
    }
}